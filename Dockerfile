FROM ubuntu:latest
# update package repositories
RUN apt-get clean && apt-get update
# install nginx
RUN apt-get -y install nginx

# set the workdir to '/var/www/html'
WORKDIR /var/www/html
# remove everything in the WORKDIR
RUN rm -rf ./*
# copy web assets to WORKDIR
COPY html/ .
#COPY html/ /var/www/html

# expose port 80
EXPOSE 80
# define nginx as entrypoint
ENTRYPOINT ["nginx", "-g", "daemon off;"]
